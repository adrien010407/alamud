# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event1

class LifeRegainEvent(Event1):
    NAME = "life-regain"

    def perform(self):
        self.actor.life = self.actor.max_life

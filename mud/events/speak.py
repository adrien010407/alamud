from .event import Event2

class SpeakEvent(Event2):
    NAME = "speak"
    
    def get_event_templates(self):
        return self.object.get_event_templates()
    
    def perform(self) : 
        self.buffer_clear()
        self.buffer_inform("speak.actor", object=self.object)
        if self.object.is_npc():
            return self.actor.send_result(self.buffer_get())

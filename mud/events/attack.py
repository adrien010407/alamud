# -*- coding: utf-8 -*-
#==============================================================================

from .event import Event2
from .death import DeathEvent
import random

class HitEvent(Event2):
    NAME = "hit"

    def perform(self):
        if not self.actor.can_see():
            return self.failed_cannot_see()
        if not self.object.is_player():
            return self.failed_not_player()
        self.inform_touch("hit")
        calculation = random.randint(self.actor.attack-int(self.actor.attack*2/10),self.actor.attack+int(self.actor.attack*2/10))-self.object.defense
        self.object.life-= calculation if calculation > 0 else 0
        if not self.object.is_alive():
            DeathEvent(self.engine, self.object).execute()
            self.inform_touch("kill")         

    def failed_not_player(self):
        self.fail()
        self.inform("hit.failed")

    def failed_cannot_see(self):
        self.fail()
        self.buffer_clear()
        self.buffer_inform("look.failed")
        self.actor.send_result(self.buffer_get())

class AttackEvent(Event2):
    NAME = "attack"

    def perform(self):
        if not self.actor.can_see():
            return self.failed_cannot_see()
        if not self.object.is_monster() and not self.object.is_player():
            return self.failed_not_ennemy()
        self.inform_touch("attack")
        calculation = random.randint(self.actor.attack-int(self.actor.attack*2/10),self.actor.attack+int(self.actor.attack*2/10))-self.object.defense
        self.object.life-= calculation if calculation > 0 else 0
        if not self.object.is_alive():
            DeathEvent(self.engine, self.object).execute()
            self.inform_touch("kill")         

    def failed_not_ennemy(self):
        self.fail()
        self.inform("attack.failed")

    def failed_cannot_see(self):
        self.fail()
        self.buffer_clear()
        self.buffer_inform("look.failed")
        self.actor.send_result(self.buffer_get())

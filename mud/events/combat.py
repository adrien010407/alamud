# -*- coding: utf-8 -*-
#==============================================================================

from .event import Event1, Event2

class CombatEvent(Event1):
    NAME = "combat"

    def get_event_templates(self):
        return self.actor.container().get_event_templates()

    def perform(self):
        if not self.actor.can_see():
            self.add_prop("cannot-see")
            return self.combat_failed()
        self.buffer_clear()
        self.buffer_inform("combat.actor", object=self.actor.container(), object2=self.actor.get_life())
        players = []
        monsters = []
        for x in self.actor.container().contents():
            if x is self.actor:
                pass
            elif x.is_player():
                players.append(x)
            elif x.is_monster():
                monsters.append(x)
        if players:
            self.buffer_inform("combat.players-intro")
            self.buffer_append("<ul>")
            for x in players:
                self.buffer_peek(x)
            self.buffer_append("</ul>")
        if monsters:
            self.buffer_inform("combat.objects-intro")
            self.buffer_append("<ul>")
            for x in monsters:
                self.buffer_peek(x)
            self.buffer_append("</ul>")
        self.actor.send_result(self.buffer_get())

    def combat_failed(self):
        self.fail()
        self.buffer_clear()
        self.buffer_inform("combat.failed.actor")
        self.actor.send_result(self.buffer_get())

class NewTurnEvent(Event2):
    NAME = "newturn"
    
    def get_event_templates(self):
        return None
    
    def perform(self):
        self.inform("combat.turn")

# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2
from .event import Event1

class TempusEvent(Event1):
    NAME = "tempus"
    
    def get_event_templates(self):
        return self.actor.container().get_event_templates()
    
    def perform(self):
        self.inform("tempus")
        
class ArchaviaEvent(Event1):
    NAME = "archavia"
    
    def get_event_templates(self):
        return self.actor.container().get_event_templates()
    
    def perform(self):
        self.inform("archavia")
        
class MundusEvent(Event1):
    NAME = "mundus"
    
    def get_event_templates(self):
        return self.actor.container().get_event_templates()
    
    def perform(self):
        self.inform("mundus")

class MagicEvent(Event2):
    NAME = "magic"
    EVENTS = {"Tempus" : TempusEvent, "Archavia" : ArchaviaEvent, "Mundus" : MundusEvent}
    
    def get_event_templates(self):
        return None

    def perform(self):
        self.EVENTS[self.object](self.engine, self.actor).execute()



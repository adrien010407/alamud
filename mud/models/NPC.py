from .thing import Thing

class NPC(Thing):
    """ A NPC is located in the world """


    #--------------------------------------------------------------------------
    # initialization
    #--------------------------------------------------------------------------
    
    def __init__(self, **kargs):
        super().__init__(**kargs)

    #--------------------------------------------------------------------------
    # initialization from YAML data
    #--------------------------------------------------------------------------

    def init_from_yaml(self, data, world):
        super().init_from_yaml(data, world)

    def update_from_yaml(self, data, world):
        super().update_from_yaml(data, world)

    #--------------------------------------------------------------------------
    # API for saving the dynamic part of objects to YAML (via JSON)
    #--------------------------------------------------------------------------

    def archive_into(self, obj):
        super().archive_into(obj)

    #--------------------------------------------------------------------------
    # model API
    #--------------------------------------------------------------------------
    
    def is_npc(self):
        return True
        
    #--------------------------------------------------------------------------
    # API used when extern action
    #--------------------------------------------------------------------------
        
    def can_see(self):
        return True
        
    def _has_prop_can_see(self):
        return self.can_see()

# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

import uuid
from .basic import Basic
import mud.game

class Fighteable(Basic):

    """mixin class that provides a life value attribute."""

    #--------------------------------------------------------------------------
    # initialization
    #--------------------------------------------------------------------------

    def __init__(self, life=None, attack=None, defense=None, agility=None, **kargs):
        if life is None or attack is None or defense is None:
            raise Exception("missing life, attack or defense: %s" % str(kargs))
        super().__init__(**kargs)
        if agility is None:
            agility = 2
        self.agility = agility
        self.life = life
        self.attack = attack
        self.defense = defense

    #--------------------------------------------------------------------------
    # initialization from YAML data
    #--------------------------------------------------------------------------

    def init_from_yaml(self, data, world):
        super().init_from_yaml(data, world)

    def update_from_yaml(self, data, world):
        super().update_from_yaml(data, world)
        self.life = data["life"]

    #--------------------------------------------------------------------------
    # API for saving the dynamic parts of objects to YAML (via JSON)
    #--------------------------------------------------------------------------

    def archive_into(self, obj):
        super().archive_into(obj)
        obj["life"] = self.life
        
    def is_alive(self):
        return self.life > 0
        
    def get_attack(self):
        return self.attack

    def get_defense(self):
        return self.defense
        
    def get_life(self):
        return self.life

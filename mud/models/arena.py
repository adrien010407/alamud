from threading import Thread
from ..events.attack import AttackEvent
from ..events.event import Event
import time

class Arena(Thread):

    def __init__(self, engine, fighters):
        Thread.__init__(self)
        self._reset = False
        self._contents = fighters #Une liste d'objet avec une vie
        self._inputs = {}
        self.engine = engine

    def run(self):
        
        while self.had_player() and self.had_monster():            
            self._reset = True
            
            while self.had_player() and self.had_monster() and self._reset:
                
                self._reset = False
                i = 0
                while self.had_player() and self.had_monster() and i < 100 and not self._reset:
                    
                    time.sleep(0.1)
                    i+=1
            
            for obj in self.get_monsters():
                self.put(obj, AttackEvent(self.engine, obj, self.get_players()[0]))
            
            def agility(playeratask):
                return playeratask[0].agility
            
            for actor,task in sorted(self._inputs.items(), key=agility, reverse=True):
                if isinstance(task, Event) and actor.is_alive():
                    task.execute()
                elif actor.is_alive():
                    self.engine.perform(task)
                
            self.update(self._contents)
            self._inputs = {}
        
    def reset(self):
        self._reset = True
        
    def add(self, fighter):
        self._contents.append(fighter)
        
    def remove(self, fighter):
        self._contents.remove(fighter)
        
    def update(self, fighters):
        self._contents = [x for x in fighters if x.is_alive()]
        self.reset()
        
    def put(self, fighter, task):
        self._inputs[fighter] = task
        
    def had_player(self):
        res = False
        i = 0
        while i < len(self._contents) and res != True:
            if self._contents[i].is_player():
                res = True
            i+=1
        return res
        
    def had_monster(self):
        res = False
        i = 0
        while i < len(self._contents) and res != True:
            if self._contents[i].is_monster():
                res = True
            i+=1
        return res
        
    def get_players(self):
        res = []
        for obj in self._contents:
            if obj.is_player():
                res.append(obj)
        return res
        
    def get_monsters(self):
        res = []
        for obj in self._contents:
            if obj.is_monster():
                res.append(obj)
        return res

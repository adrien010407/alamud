from .NPC import NPC
from .mixins.fight import Fighteable
from .mixins.containing import Containing
from .model import Model

class Monster(NPC, Fighteable, Containing):
    """ A Monster is a NCP """


    #--------------------------------------------------------------------------
    # initialization
    #--------------------------------------------------------------------------
    
    def __init__(self, **kargs):
        super().__init__(**kargs)

    #--------------------------------------------------------------------------
    # initialization from YAML data
    #--------------------------------------------------------------------------

    def init_from_yaml(self, data, world):
        super().init_from_yaml(data, world)
        self.det = ("le ","un ")
        if self.gender == "feminine":
            self.det = ("la ","une ")

    def update_from_yaml(self, data, world):
        super().update_from_yaml(data, world)

    #--------------------------------------------------------------------------
    # API for saving the dynamic part of objects to YAML (via JSON)
    #--------------------------------------------------------------------------

    def archive_into(self, obj):
        super().archive_into(obj)

    #--------------------------------------------------------------------------
    # model API
    #--------------------------------------------------------------------------
    
    def is_player(self):
        return False
    
    def is_monster(self):
        return True
        
    def noun_the(self):
        return self.det[0]+self.name

    def noun_a(self):
        return self.det[1]+self.name
        

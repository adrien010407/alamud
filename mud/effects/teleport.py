# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect2
from mud.events import TeleportEvent

class TeleportEffect(Effect2):
    EVENT = TeleportEvent
    
    def resolve_object(self):
        return 

    def make_event(self):
        from mud.game import GAME
        try:
            locs = [GAME.world[self.yaml["location"]]]
        except:
            locs = [self.actor]
        try:
            actor = GAME.world[self.yaml["id"]]
        except:
            actor = self.actor
        return self.EVENT(self.engine, actor, locs)

# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from mud.actions import (
    GoAction, TakeAction, LookAction, InspectAction, OpenAction,
    OpenWithAction, CloseAction, TypeAction, InventoryAction,
    LightOnAction, LightOffAction, DropAction, DropInAction,
    PushAction, TeleportAction, EnterAction, LeaveAction, ReadAction,
    HitAction, AttackAction, SpeakAction, MagicAction
)

import mud.game

def make_rules():
    GAME = mud.game.GAME
    DIRS = list(GAME.static["directions"]["noun_at_the"].values())
    DIRS.extend(GAME.static["directions"]["noun_the"].values())
    DIRS.extend(GAME.static["directions"]["normalized"].keys())
    DETS = "(?:l |le |la |les |une |un |L |Le |La |Les |Une |Un |)"
    ATS = "(?:a |à |A |À |au |Au |)"

    return (
        (GoAction       , r"(?:[aA]ller |)(%s)" % "|".join(DIRS)),
        (TakeAction     , r"(?:p|P|[pP]rendre) %s([\S+\s?]+)" % DETS),
        (LookAction     , r"(?:r|R|[Rr]egarder)"),
        (InspectAction  , r"(?:r|R|[Rr]egarder|[iI]nspecter|[oO]bserver) %s([\S+\s?]+)" % DETS),
        (OpenAction     , r"[Oo]uvrir %s(\S+)" % DETS),
        (OpenWithAction , r"[Oo]uvrir %s(\S+) [aA]vec %s(\w+)" % (DETS,DETS)),
        (CloseAction    , r"[Ff]ermer %s(\S+)" % DETS),
        (TypeAction     , r"(?:[Tt]aper|[EÉeé]crire) (\S+)$"),
        (InventoryAction, r"(?:[iI]nventaire|[iI]nv|[iI])$"),
        (LightOnAction  , r"[aA]llumer %s(\S+)" % DETS),
        (LightOffAction , r"[EÉeé]teindre %s(\S+)" % DETS),
        (DropAction     , r"(?:[pP]oser|laisser) %s([\S+\s?]+)" % DETS),
        (DropInAction   , r"(?:[pP]oser|laisser) %s([\S+\s?]+) (?:[dD]ans |[sS]ur |)%s(\S+)" % (DETS,DETS)),
        (PushAction     , r"(?:[aA]ppuyer|[pP]ousser|[pP]resser)(?: [sS]ur|) %s(\S+)" % DETS),
        (TeleportAction , r"[Tt]ele(?:porter|) (\S+)"),
        (EnterAction    , r"[Ee]ntrer"),
        (LeaveAction    , r"[Ss]ortir|[pP]artir"),
        (ReadAction     , r"(?:l|L|[Ll]ire) %s([\S+\s?]+)" % DETS),
        (HitAction      , r"[Ff]rapper (\S+)"),
        (AttackAction   , r"(?:a|A|[Aa]ttaquer) %s([\S+\s?]+)" % DETS),
        (SpeakAction    , r"(?:s|S|[Pp]arler) %s%s([\S+\s?]+)" % (ATS,DETS)),
        (MagicAction    , r"(?:u|U|d|D|[uU]tiliser|[dD]ire) (\S+)"),
    )
